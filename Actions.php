<?php
/**
 * Actions
 *
 * @package NFC/QR Actions module
 */

require_once 'ProgramFunctions/Substitutions.fnc.php';
require_once 'ProgramFunctions/Template.fnc.php';
require_once 'ProgramFunctions/MarkDownHTML.fnc.php';
require_once 'modules/NFC_QR_Actions/includes/Actions.fnc.php';

DrawHeader( ProgramTitle() );

if ( $_REQUEST['modfunc'] === 'update' )
{
	if ( ! empty( $_REQUEST['values'] )
		&& ! empty( $_POST['values'] )
		&& AllowEdit() )
	{
		foreach ( (array) $_REQUEST['values'] as $id => $columns )
		{
			if ( isset( $columns['URL_STUDENT'] ) )
			{
				// Remove RosarioSIS URL from URL.
				$columns['URL_STUDENT'] = str_replace(
					RosarioURL(),
					'',
					$columns['URL_STUDENT']
				);
			}

			if ( isset( $columns['URL_STAFF'] ) )
			{
				// Remove RosarioSIS URL from URL.
				$columns['URL_STAFF'] = str_replace(
					RosarioURL(),
					'',
					$columns['URL_STAFF']
				);
			}

			if ( $id !== 'new' )
			{
				DBUpdate(
					'nfc_qr_actions',
					$columns,
					[ 'ID' => (int) $id ]
				);
			}

			// New: check for Title.
			elseif ( $columns['TITLE'] )
			{
				DBInsert(
					'nfc_qr_actions',
					$columns
				);
			}
		}
	}

	// Unset modfunc, values & redirect URL.
	RedirectURL( [ 'modfunc' , 'values' ] );
}

if ( $_REQUEST['modfunc'] === 'update_configuration' )
{
	if ( ! empty( $_REQUEST['values'] )
		&& ! empty( $_POST['values'] )
		&& ! empty( $_REQUEST['id'] )
		&& AllowEdit() )
	{
		if ( empty( $_REQUEST['display_template_student'] ) )
		{
			$columns['STUDENT_TEMPLATE'] = '';
		}
		elseif ( isset( $_POST['values']['STUDENT_TEMPLATE'] ) )
		{
			$columns['STUDENT_TEMPLATE'] = SanitizeHTML( $_POST['values']['STUDENT_TEMPLATE'] );
		}

		if ( empty( $_REQUEST['display_template_staff'] ) )
		{
			$columns['STAFF_TEMPLATE'] = '';
		}
		elseif ( isset( $_POST['values']['STAFF_TEMPLATE'] ) )
		{
			$columns['STAFF_TEMPLATE'] = SanitizeHTML( $_POST['values']['STAFF_TEMPLATE'] );
		}

		DBUpdate(
			'nfc_qr_actions',
			$columns,
			[ 'ID' => (int) $_REQUEST['id'] ]
		);
	}

	// Unset modfunc, values & redirect URL.
	RedirectURL( [ 'modfunc' , 'values' ] );
}

if ( $_REQUEST['modfunc'] === 'remove'
	&& AllowEdit() )
{
	if ( DeletePrompt( dgettext( 'NFC_QR_Actions', 'Action' ) ) )
	{
		DBQuery( "DELETE FROM nfc_qr_staff_actions
			WHERE ACTION_ID='" . (int) $_REQUEST['id'] . "'" );

		DBQuery( "DELETE FROM nfc_qr_actions
			WHERE ID='" . (int) $_REQUEST['id'] . "'" );

		// Unset modfunc & ID & redirect URL.
		RedirectURL( [ 'modfunc', 'id' ] );
	}
}

if ( ! $_REQUEST['modfunc'] )
{
	if ( ! empty( $_REQUEST['id'] )
		&& DBGetOne( "SELECT 1
			FROM nfc_qr_actions
			WHERE ID='" . (int) $_REQUEST['id'] . "'" ) )
	{
		$action_RET = DBGet( "SELECT TITLE,URL_STUDENT,URL_STAFF,STUDENT_TEMPLATE,STAFF_TEMPLATE
			FROM nfc_qr_actions
			WHERE ID='" . (int) $_REQUEST['id'] . "'" );

		echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] .
				'&id=' . $_REQUEST['id'] . '&modfunc=update_configuration' ) . '" method="POST">';

		// Configuration.
		DrawHeader(
			'<a href="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] ) . '">« ' . _( 'Back' ) . '</a> ' .
			$action_RET[1]['TITLE'],
			SubmitButton()
		);

		if ( ! empty( $action_RET[1]['URL_STUDENT'] ) )
		{
			if ( ! empty( $action_RET[1]['STUDENT_TEMPLATE'] ) )
			{
				$_REQUEST['display_template_student'] = 'Y';
			}

			$template_header = NFCQRActionsDisplayTemplateCheckbox(
				issetVal( $_REQUEST['display_template_student'], '' ),
				'display_template_student'
			);

			if ( $_REQUEST['display_template_student'] )
			{
				$student_template = $action_RET[1]['STUDENT_TEMPLATE'];

				if ( ! $student_template )
				{
					$student_template = GetTemplate( 'NFC_QR_Actions/Actions.php&STUDENT_TEMPLATE' );
				}

				$template_header .= '<br />' .
					NFCQRActionsTemplateInput( $student_template, 'values[STUDENT_TEMPLATE]' );
			}

			DrawHeader( $template_header );
		}

		if ( ! empty( $action_RET[1]['URL_STAFF'] ) )
		{
			if ( ! empty( $action_RET[1]['STAFF_TEMPLATE'] ) )
			{
				$_REQUEST['display_template_staff'] = 'Y';
			}

			$template_header = NFCQRActionsDisplayTemplateCheckbox(
				issetVal( $_REQUEST['display_template_staff'], '' ),
				'display_template_staff'
			);

			if ( $_REQUEST['display_template_staff'] )
			{
				$staff_template = $action_RET[1]['STAFF_TEMPLATE'];

				if ( ! $staff_template )
				{
					$staff_template = GetTemplate( 'NFC_QR_Actions/Actions.php&STAFF_TEMPLATE' );
				}

				$template_header .= '<br />' .
					NFCQRActionsTemplateInput( $staff_template, 'values[STAFF_TEMPLATE]' );
			}

			DrawHeader( $template_header );
		}

		echo '<br /><div class="center">' . SubmitButton() . '</div></form>';
	}
	else
	{
		$resources_RET = DBGet( "SELECT ID,TITLE,URL_STUDENT,URL_STAFF,ID AS CONFIGURATION
			FROM nfc_qr_actions
			ORDER BY TITLE,ID",
		[
			'TITLE' => 'NFCQRActionsMakeTextInput',
			'URL_STUDENT' => 'NFCQRActionsMakeURL',
			'URL_STAFF' => 'NFCQRActionsMakeURL',
			'CONFIGURATION' => 'NFCQRActionsMakeConfiguration',
		] );

		$columns = [
			'TITLE' => _( 'Title' ),
			'URL_STUDENT' => dgettext( 'NFC_QR_Actions', 'URL (Student)' ),
			'URL_STAFF' => dgettext( 'NFC_QR_Actions', 'URL (User)' ),
			'CONFIGURATION' => _( 'Configuration' ),
		];

		$link['add']['html'] = [
			'TITLE' => NFCQRActionsMakeTextInput( '', 'TITLE' ),
			'URL_STUDENT' => NFCQRActionsMakeURL( '', 'URL_STUDENT' ),
			'URL_STAFF' => NFCQRActionsMakeURL( '', 'URL_STAFF' ),
			'CONFIGURATION' => '',
		];

		$link['remove']['link'] = 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=remove';
		$link['remove']['variables'] = [ 'id' => 'ID' ];

		echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=update' ) . '" method="POST">';

		DrawHeader( '', SubmitButton() );

		$note[] = dgettext(
			'NFC_QR_Actions',
			'URL (Student): replace the student ID with <code>__STUDENT_ID__</code> in the URL'
		);

		$note[] = dgettext(
			'NFC_QR_Actions',
			'URL (User): replace the user ID with <code>__STAFF_ID__</code> in the URL'
		);

		echo ErrorMessage( $note, 'note' );

		ListOutput(
			$resources_RET,
			$columns,
			dgettext( 'NFC_QR_Actions', 'Action' ),
			dgettext( 'NFC_QR_Actions', 'Actions' ),
			$link
		);

		echo '<div class="center">' . SubmitButton() . '</div>';
		echo '</form>';
	}
}
