<?php
/**
 * Generate a 300x300 black & white QR code image
 * Pass url param to encode an URL
 * URL must contain 'Modules.php?modname='
 * Pass label param (optional) to add text below QR code
 *
 * @uses https://github.com/endroid/qr-code version 1.9.3 (PHP5.5) or version 5.0.7 (PHP8.1)
 *
 * @example <img src="http://localhost/gitlab/rosariosis/modules/NFC_QR_Actions/qrcode_image.php?url=http%3A%2F%2Flocalhost%2Fgitlab%2Frosariosis%2FModules.php%3Fmodname%3DStudents%2FStudent.php%26student_id%3D1%26category_id%3D17" />
 */

use Endroid\QrCode\QrCode;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\Label\Label;
use Endroid\QrCode\Label\Font\OpenSans;
use Endroid\QrCode\Writer\PngWriter;
use Endroid\QrCode\RoundBlockSizeMode;

if ( empty( $_REQUEST['url'] )
	|| ! filter_var( $_REQUEST['url'], FILTER_VALIDATE_URL )
	|| mb_strpos( $_REQUEST['url'], 'Modules.php?modname=' ) === false )
{
	die( 'Error: Not a valid URL' );
}

if ( version_compare( PHP_VERSION, '8.1', '<' ) )
{
	// PHP < 8.1, endroid/qr-code 1.9
	// Compatible down to PHP 5.5
	require_once 'vendor-php55/autoload.php';

	$qr_code = new QrCode();
	$qr_code
		->setText( $_REQUEST['url'] )
		->setSize( 280 ) // 300x300 = 150x150 (Student Photo) scale 2x.
		->setPadding( 10 )
		->setErrorCorrection( 'high' )
		->setForegroundColor( ['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0] )
		->setBackgroundColor( ['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0] )
		->setLabel( ! empty( $_REQUEST['label'] ) ? $_REQUEST['label'] : '' )
		->setImageType( QrCode::IMAGE_TYPE_PNG )
	;

	// now we can directly output the qrcode
	header( 'Content-Type: ' . $qr_code->getContentType() );
	$qr_code->render();
}
else
{
	/**
	 * PHP >= 8.1, endroid/qr-code 5.0.7
	 * Warning: really slow with xdebug...
	 *
	 * @link https://github.com/endroid/qr-code/issues/444
	 */
	require_once 'vendor/autoload.php';

	$qrCode = QrCode::create( $_REQUEST['url'] )
		->setEncoding( new Encoding( 'UTF-8' ) )
		->setErrorCorrectionLevel( ErrorCorrectionLevel::High )
		->setSize( 280 ) // 300x300 = 150x150 (Student Photo) scale 2x.
		->setMargin( 10 )
		->setForegroundColor( new Color( 0, 0, 0 ) )
		->setBackgroundColor( new Color( 255, 255, 255 ) )
		->setRoundBlockSizeMode( RoundBlockSizeMode::None );

	// Create generic label
	$label = Label::create( ! empty( $_REQUEST['label'] ) ? $_REQUEST['label'] : '' )
		/**
		 * Use OpenSans font
		 * Noto (default) was removed, too big!
		 *
		 * @see vendor/endroid/qr-code/assets/noto_sans.otf
		 */
		->setFont( new OpenSans( 16 ) )
		->setTextColor( new Color( 0, 0, 0 ) );

	$writer = new PngWriter();
	$result = $writer->write( $qrCode, null, $label );

	// Directly output the QR code
	header( 'Content-Type: ' . $result->getMimeType() );
	echo $result->getString();
}
