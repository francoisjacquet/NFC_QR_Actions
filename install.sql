/**
 * Install PostgreSQL
 * Required if the module has menu entries
 * - Add profile exceptions for the module to appear in the menu
 * - Add program config options if any (to every schools)
 * - Add module specific tables (and their eventual sequences & indexes)
 *   if any: see rosariosis.sql file for examples
 *
 * @package NFC/QR Actions
 */

 -- Fix #102 error language "plpgsql" does not exist
 -- http://timmurphy.org/2011/08/27/create-language-if-it-doesnt-exist-in-postgresql/
 --
 -- Name: create_language_plpgsql(); Type: FUNCTION; Schema: public; Owner: postgres
 --

CREATE FUNCTION create_language_plpgsql()
RETURNS BOOLEAN AS $$
    CREATE LANGUAGE plpgsql;
    SELECT TRUE;
$$ LANGUAGE SQL;

SELECT CASE WHEN NOT (
    SELECT TRUE AS exists FROM pg_language
    WHERE lanname='plpgsql'
    UNION
    SELECT FALSE AS exists
    ORDER BY exists DESC
    LIMIT 1
) THEN
    create_language_plpgsql()
ELSE
    FALSE
END AS plpgsql_created;

DROP FUNCTION create_language_plpgsql();


/**
 * profile_exceptions Table
 *
 * profile_id:
 * - 0: student
 * - 1: admin
 * - 2: teacher
 * - 3: parent
 * modname: should match the Menu.php entries
 * can_use: 'Y'
 * can_edit: 'Y' or null (generally null for non admins)
 */
--
-- Data for Name: profile_exceptions; Type: TABLE DATA;
--

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'NFC_QR_Actions/Actions.php', 'Y', 'Y'
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='NFC_QR_Actions/Actions.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'NFC_QR_Actions/URLCodes.php', 'Y', 'Y'
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='NFC_QR_Actions/URLCodes.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'NFC_QR_Actions/MyActions.php', 'Y', 'Y'
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='NFC_QR_Actions/MyActions.php'
    AND profile_id=1);


--
-- Name: nfc_qr_actions; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_table_nfc_qr_actions() RETURNS void AS
$func$
BEGIN
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables
        WHERE schemaname = CURRENT_SCHEMA()
        AND tablename = 'nfc_qr_actions') THEN
    RAISE NOTICE 'Table "nfc_qr_actions" already exists.';
    ELSE
        CREATE TABLE nfc_qr_actions (
            id serial PRIMARY KEY,
            title varchar(100) NOT NULL,
            url_student text,
            url_staff text,
            student_template text,
            staff_template text,
            created_at timestamp DEFAULT current_timestamp,
            updated_at timestamp
        );

        CREATE TRIGGER set_updated_at
            BEFORE UPDATE ON nfc_qr_actions
            FOR EACH ROW EXECUTE PROCEDURE set_updated_at();
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_table_nfc_qr_actions();
DROP FUNCTION create_table_nfc_qr_actions();


--
-- Name: nfc_qr_staff_actions; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_table_nfc_qr_staff_actions() RETURNS void AS
$func$
BEGIN
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables
        WHERE schemaname = CURRENT_SCHEMA()
        AND tablename = 'nfc_qr_staff_actions') THEN
    RAISE NOTICE 'Table "nfc_qr_staff_actions" already exists.';
    ELSE
        CREATE TABLE nfc_qr_staff_actions (
            id serial PRIMARY KEY,
            action_id integer NOT NULL REFERENCES nfc_qr_actions(id),
            staff_id integer NOT NULL REFERENCES staff(staff_id),
            close_tab_after_seconds integer,
            created_at timestamp DEFAULT current_timestamp,
            updated_at timestamp
        );

        CREATE TRIGGER set_updated_at
            BEFORE UPDATE ON nfc_qr_staff_actions
            FOR EACH ROW EXECUTE PROCEDURE set_updated_at();
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_table_nfc_qr_staff_actions();
DROP FUNCTION create_table_nfc_qr_staff_actions();


/*********************************************************
 Add Student template
**********************************************************/
--
-- Data for Name: templates; Type: TABLE DATA;
--

INSERT INTO templates (modname, staff_id, template)
SELECT 'NFC_QR_Actions/Actions.php&STUDENT_TEMPLATE', 0, '<table style="border-collapse: collapse; width: 100%; max-width: 480px;" border="1">
<tbody>
<tr>
<td style="width: 150px;">__PHOTO__</td>
<td>
<h2>__FULL_NAME__</h2>
<p>ID: __STUDENT_ID__</p>
<p>Grade Level: __GRADE_ID__</p>
</td>
</tr>
</tbody>
</table>
<p><a href="Modules.php?modname=Students/Student.php&amp;student_id=__STUDENT_ID__">Student Info</a></p>'
WHERE NOT EXISTS (SELECT modname
    FROM templates
    WHERE modname='NFC_QR_Actions/Actions.php&STUDENT_TEMPLATE'
    AND staff_id=0);


/*********************************************************
 Add User template
**********************************************************/
--
-- Data for Name: templates; Type: TABLE DATA;
--

INSERT INTO templates (modname, staff_id, template)
SELECT 'NFC_QR_Actions/Actions.php&STAFF_TEMPLATE', 0, '<table style="border-collapse: collapse; width: 100%; max-width: 480px;" border="1">
<tbody>
<tr>
<td style="width: 150px;">__PHOTO__</td>
<td>
<h2>__FULL_NAME__</h2>
<p>ID: __STAFF_ID__</p>
<p>Profile: __PROFILE__</p>
</td>
</tr>
</tbody>
</table>
<p><a href="Modules.php?modname=Users/User.php&amp;staff_id=__STAFF_ID__">User Info</a></p>'
WHERE NOT EXISTS (SELECT modname
    FROM templates
    WHERE modname='NFC_QR_Actions/Actions.php&STAFF_TEMPLATE'
    AND staff_id=0);
