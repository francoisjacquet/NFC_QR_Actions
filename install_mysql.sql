/**
 * Install MySQL
 * Required if the module has menu entries
 * - Add profile exceptions for the module to appear in the menu
 * - Add program config options if any (to every schools)
 * - Add module specific tables (and their eventual sequences & indexes)
 *   if any: see rosariosis.sql file for examples
 *
 * @package NFC/QR Actions
 */


/**
 * profile_exceptions Table
 *
 * profile_id:
 * - 0: student
 * - 1: admin
 * - 2: teacher
 * - 3: parent
 * modname: should match the Menu.php entries
 * can_use: 'Y'
 * can_edit: 'Y' or null (generally null for non admins)
 */
--
-- Data for Name: profile_exceptions; Type: TABLE DATA;
--

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'NFC_QR_Actions/Actions.php', 'Y', 'Y'
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='NFC_QR_Actions/Actions.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'NFC_QR_Actions/URLCodes.php', 'Y', 'Y'
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='NFC_QR_Actions/URLCodes.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'NFC_QR_Actions/MyActions.php', 'Y', 'Y'
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='NFC_QR_Actions/MyActions.php'
    AND profile_id=1);


--
-- Name: nfc_qr_actions; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE TABLE IF NOT EXISTS nfc_qr_actions (
    id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title varchar(100) NOT NULL,
    url_student text,
    url_staff text,
    student_template text,
    staff_template text,
    created_at timestamp DEFAULT current_timestamp,
    updated_at timestamp NULL ON UPDATE current_timestamp
);


--
-- Name: nfc_qr_staff_actions; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE TABLE IF NOT EXISTS nfc_qr_staff_actions (
    id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
    action_id integer NOT NULL,
    FOREIGN KEY (action_id) REFERENCES nfc_qr_actions(id),
    staff_id integer NOT NULL,
    FOREIGN KEY (staff_id) REFERENCES staff(staff_id),
    close_tab_after_seconds integer,
    created_at timestamp DEFAULT current_timestamp,
    updated_at timestamp NULL ON UPDATE current_timestamp
);


/*********************************************************
 Add Student template
**********************************************************/
--
-- Data for Name: templates; Type: TABLE DATA;
--

INSERT INTO templates (modname, staff_id, template)
SELECT 'NFC_QR_Actions/Actions.php&STUDENT_TEMPLATE', 0, '<table style="border-collapse: collapse; width: 100%; max-width: 480px;" border="1">
<tbody>
<tr>
<td style="width: 150px;">__PHOTO__</td>
<td>
<h2>__FULL_NAME__</h2>
<p>ID: __STUDENT_ID__</p>
<p>Grade Level: __GRADE_ID__</p>
</td>
</tr>
</tbody>
</table>
<p><a href="Modules.php?modname=Students/Student.php&amp;student_id=__STUDENT_ID__">Student Info</a></p>'
FROM DUAL
WHERE NOT EXISTS (SELECT modname
    FROM templates
    WHERE modname='NFC_QR_Actions/Actions.php&STUDENT_TEMPLATE'
    AND staff_id=0);


/*********************************************************
 Add User template
**********************************************************/
--
-- Data for Name: templates; Type: TABLE DATA;
--

INSERT INTO templates (modname, staff_id, template)
SELECT 'NFC_QR_Actions/Actions.php&STAFF_TEMPLATE', 0, '<table style="border-collapse: collapse; width: 100%; max-width: 480px;" border="1">
<tbody>
<tr>
<td style="width: 150px;">__PHOTO__</td>
<td>
<h2>__FULL_NAME__</h2>
<p>ID: __STAFF_ID__</p>
<p>Profile: __PROFILE__</p>
</td>
</tr>
</tbody>
</table>
<p><a href="Modules.php?modname=Users/User.php&amp;staff_id=__STAFF_ID__">User Info</a></p>'
FROM DUAL
WHERE NOT EXISTS (SELECT modname
    FROM templates
    WHERE modname='NFC_QR_Actions/Actions.php&STAFF_TEMPLATE'
    AND staff_id=0);
