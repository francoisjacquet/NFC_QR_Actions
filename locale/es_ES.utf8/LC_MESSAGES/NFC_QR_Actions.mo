��            )   �      �     �     �     �     �     �     �     �       
             #     7     H  %   W  "   }     �  .   �  +   �          $     +  &   <     c     l  Q   z  
   �  I   �  =   !  �  _     I     Q  -   Z  )   �     �  "   �     �     �     �               !     5  8   E  5   ~  (   �  <   �  9   	     T	     `	     g	  2   	  	   �	     �	  [   �	     )
  S   7
  U   �
                                                                       
                                                  	                 Action Actions Cancel automatic tab closing Close tab after (seconds) Copied Download QR codes as zip External URL Invalid URL My Actions NFC URL NFC URL or QR codes NFC URL/QR codes NFC/QR Actions No URL (Student) set for this action. No URL (User) set for this action. No possible actions were found. Perform action and display student information Perform action and display user information QR codes Remove Select an action Set as default action for this session Template URL (Student) URL (Student): replace the student ID with <code>__STUDENT_ID__</code> in the URL URL (User) URL (User): replace the user ID with <code>__STAFF_ID__</code> in the URL Write the URL to a NFC tag using third-party software or app. Project-Id-Version: NFC/QR Actions module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-11-29 19:44+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.2
X-Poedit-SearchPath-0: .
 Acción Acciones Cancelar el cierre automático de la pestaña Cerrar la pestaña después de (segundos) Copiado Descargar los códigos QR como zip URL externa URL invalida Mis acciones URL NFC URL NFC o códigos QR URL NFC/códigos QR Acciones NFC/QR La URL (estudiante) no está definida para esta acción. La URL (usuario) no está definida para esta acción. No se encontró ninguna acción posible. Realizar la acción y mostrar la información del estudiante Realizar la acción y mostrar la información del usuario Códigos QR Quitar Seleccionar una acción Definir como acción por defecto para esta sesión Plantilla URL (estudiante) URL (estudiante): reemplazar el ID del estudiante por <code>__STUDENT_ID__</code> en la URL URL (usuario) URL (usuario): reemplazar el ID del usuario por <code>__STAFF_ID__</code> en la URL Escriba la URL en una tarjeta NFC mediante un software o una aplicación de terceros. 