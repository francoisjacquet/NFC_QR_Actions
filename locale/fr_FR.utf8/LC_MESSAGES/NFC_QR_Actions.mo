��            )   �      �     �     �     �     �     �     �     �       
             #     7     H  %   W  "   }     �  .   �  +   �          $     +  &   <     c     l  Q   z  
   �  I   �  =   !  �  _     H     O  ,   W  !   �     �  $   �     �     �     �     �     �          $  0   3  4   d      �  <   �  @   �     8	     A	     I	  4   b	     �	     �	  Z   �	     
  `   
  L   {
                                                                       
                                                  	                 Action Actions Cancel automatic tab closing Close tab after (seconds) Copied Download QR codes as zip External URL Invalid URL My Actions NFC URL NFC URL or QR codes NFC URL/QR codes NFC/QR Actions No URL (Student) set for this action. No URL (User) set for this action. No possible actions were found. Perform action and display student information Perform action and display user information QR codes Remove Select an action Set as default action for this session Template URL (Student) URL (Student): replace the student ID with <code>__STUDENT_ID__</code> in the URL URL (User) URL (User): replace the user ID with <code>__STAFF_ID__</code> in the URL Write the URL to a NFC tag using third-party software or app. Project-Id-Version: NFC/QR Actions module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-11-29 19:45+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.4.2
X-Poedit-SearchPath-0: .
 Action Actions Annuler la fermeture automatique de l'onglet Fermer l'onglet après (secondes) Copié Télécharger les QR codes comme zip URL externe URL invalide Mes actions URL NFC URL NFC ou QR codes URL NFC/QR codes Actions NFC/QR Aucune URL (élève) définie pour cette action. Aucune URL (utilisateur) définie pour cette action. Aucune action possible trouvée. Accomplir l'action et afficher les informations de l'élève Accomplir l'action et afficher les informations de l'utilisateur QR codes Retirer Sélectionner une action Définir comme action par défaut pour cette session Modèle URL (élève) URL (élève) : remplacer l'ID élève avec le code <code>__STUDENT_ID__</code> dans l'URL URL (utilisateur) URL (utilisateur) : remplacer l'ID utilisateur avec le code <code>__STAFF_ID__</code> dans l'URL Écrivez l'URL sur une carte NFC avec un logiciel ou une application tierce. 