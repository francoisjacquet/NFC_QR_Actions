��          �   %   �      p     q     y     �     �     �     �     �  
   �     �     �          !  %   0  "   V     y  .   �  +   �     �     �       &        <     E  Q   S  
   �  I   �  =   �  �  8  
   !  &   ,      S     t     }     �     �     �     �     �     �     �            "   :  .   ]  2   �     �     �     �     �     �     �  M   		     W	  G   g	  <   �	                         	                                   
                                                               Actions Cancel automatic tab closing Close tab after (seconds) Copied Download QR codes as zip External URL Invalid URL My Actions NFC URL NFC URL or QR codes NFC URL/QR codes NFC/QR Actions No URL (Student) set for this action. No URL (User) set for this action. No possible actions were found. Perform action and display student information Perform action and display user information QR codes Remove Select an action Set as default action for this session Template URL (Student) URL (Student): replace the student ID with <code>__STUDENT_ID__</code> in the URL URL (User) URL (User): replace the user ID with <code>__STAFF_ID__</code> in the URL Write the URL to a NFC tag using third-party software or app. Project-Id-Version: NFC/QR Actions module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-12-12 15:26+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.4.1
X-Poedit-SearchPath-0: .
 Nastavitve Prekliči samodejno zapiranje zavihkov Zapri pojavno okno po (sekundah) Kopirano Prenesite kode QR kot zip Zunanji URL Napačen URL Moje nastavitve URL NFC URL-ji NFC ali kode QR URL NFC/QR kode NFC/QR Nastavitve URL ni nastavljen (dijak). URL ni nastavljen (uporabnik) Nobene nastavitve ni bilo najdene. Izvedi akcijo in prikaži dijakove informacije Izvedi akcijo in prikaži uporabnikove informacije QR kode Odstrani Izberi nastavitev Prednastavi za to Predloga URL (dijak) URL (dijak) : Zamenjaj dijakov ID z <code>__STUDENT_ID__</code> v URL naslovu URL (uporabnik) URL (uporabnik) : zamenjaj ID z <code>__STAFF_ID__</code> v URL naslovu Uporabi drugo programsko opremo za zapis URL na NFC kartico. 