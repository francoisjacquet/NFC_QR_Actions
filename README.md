NFC/QR Actions module
=====================

![screenshot](https://gitlab.com/francoisjacquet/NFC_QR_Actions/raw/master/screenshot.png?inline=false)

https://www.rosariosis.org/modules/nfc-qr-actions/

Version 1.8 - February, 2025

License GNU/GPLv2 or later

Author François Jacquet

Sponsored by AT Group, Slovenia

DESCRIPTION
-----------
NFC/QR Actions module for RosarioSIS. Scan the QR code or the NFC tag to automatically perform actions. A new browser tab will open (requires an open administrator user session).

The _NFC URL/QR codes_ program lists
- URLs to write to NFC tags, using third party software such as
  - GoToTags [desktop](https://learn.gototags.com/desktop-app), [steps here](https://gitlab.com/francoisjacquet/NFC_QR_Actions/-/tree/master/GoToTags) (or batch [encoder](https://gototags.com/encoder))
  - [NFC Tools](https://www.wakdev.com/en/knowledge-base/how-to-guides/how-to-write-a-link-url-on-an-nfc-chip.html) Android/iOS app
- QR codes to print or save on smartphones.

Each URL is unique to a student or user (parent, teacher or other) and is used for as many actions defined by the _Actions_ program. Each action consist of a URL (typically a RosarioSIS program, can also be external) where student/user ID is dynamically replaced. When the URL/action is triggered, the result is shown, or instead, student/user information (template) can be displayed.

Once possible actions are defined, each administrator (or teacher) can select their own actions from the _My Actions_ program. For each action, you can set a number of seconds after which the browser tab is automatically closed.

Note: You can give students or parents access to the "NFC URL/QR codes" program so they can print their own QR code / write their own NFC tag.

Note 2: smartphones are able to read NFC tags / QR codes and open the URL without the need of an additional app.

Note 3: Staff ID changes (_Rollover_) from one school year to another, and so does the URL.

Note 4: on desktop, [GoToTags](https://learn.gototags.com/desktop-app/download) (paid) was successfully tested. It can open the URL in the browser when a NFC tag is read. NFC Tools (free) was also tested but it won't open the browser. [nfc4pc](https://github.com/martinpaljak/NFC4PC/wiki) (open source, not tested) is a simple Java program to open the URL in the browser just like smartphones do. Disclaimer: for support, please contact third party software vendors directly.

Translated in [French](https://www.rosariosis.org/frmodules/nfc-qr-actions/), [Spanish](https://www.rosariosis.org/es/modules/nfc-qr-actions/) and Slovenian.

ACTION & URL EXAMPLES
---------------------
Here are examples of possible actions:

Simply display the _Student / User Info_ ("General Info" tab)
```
Modules.php?modname=Students/Student.php&category_id=1&student_id=__STUDENT_ID__
Modules.php?modname=Users/User.php&category_id=1&staff_id=__STAFF_ID__
```

Serve Meals (_Food Service_ module)
```
Modules.php?modname=Food_Service/ServeMenus.php&type=student&student_id=__STUDENT_ID__
Modules.php?modname=Users/User.php&category_id=1&type=staff&staff_id=__STAFF_ID__
```

Serve Meals and preselect items based on user reservation. See the [Food Service Premium](https://www.rosariosis.org/modules/food-service-premium/) module for examples.

Record an entry (at a checkpoint) or a package delivery. See the [Entry and Exit](https://www.rosariosis.org/modules/entry-exit/) module for examples.

Return document in one click or lend a document (Quick Loan) to the selected user or student. See the [Library Premium](https://www.rosariosis.org/modules/library/) module for example.

External URL with template: in case the URL is on another domain/host and you have set a template, the AJAX request will be forbidden due to [CORS](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing). In this case, try to move the script on your server (same-origin).

External URL without template: RosarioSIS will redirect the browser (ie. not inside a frame). The "Close tab after (seconds)" option will not be available in this case.

More actions such as taking attendance are possible. Please get in touch if your school wants to sponsor new ones.

CONTENT
-------
NFC/QR Actions
- My Actions
- Actions
- NFC URL/QR codes

INSTALL
-------
Copy the `NFC_QR_Actions/` folder (if named `NFC_QR_Actions-master`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School > Configuration > Modules_ and click "Activate".

Requires RosarioSIS 11.2+
