<?php
/**
 * NFC URL/QR codes (Users)
 *
 * @package NFC/QR Actions
 */

$_REQUEST['url_or_codes'] = issetVal( $_REQUEST['url_or_codes'], '' );

if ( $_REQUEST['search_modfunc'] === 'list' )
{
	echo '<form action="' . PreparePHP_SELF( [], [ 'url_or_codes' ] ) . '" method="GET">';

	if ( ! AllowEdit() )
	{
		$_ROSARIO['allow_edit'] = true;

		$allow_edit_tmp = true;
	}

	$qr_code_download_header = '';

	if ( $_REQUEST['url_or_codes']
		&& User( 'PROFILE' ) !== 'parent' )
	{
		$qr_code_download_header = NFCQRActionsDownloadQRCodesButton();
	}

	DrawHeader( NFCQRActionsURLCodesSelect(), $qr_code_download_header );

	if ( ! empty( $allow_edit_tmp ) )
	{
		$_ROSARIO['allow_edit'] = false;
	}

	echo '</form>';

	if ( ! $_REQUEST['url_or_codes'] )
	{
		$note[] = dgettext( 'NFC_QR_Actions', 'Write the URL to a NFC tag using third-party software or app.' );
	}
}

echo ErrorMessage( $note, 'note' );

$extra['columns_after'] = [
	'URL_OR_CODES' => ( ! $_REQUEST['url_or_codes'] ?
		dgettext( 'NFC_QR_Actions', 'NFC URL' ) : dgettext( 'NFC_QR_Actions', 'QR codes' ) ),
];

$extra['SELECT'] = ",s.CREATED_AT AS URL_OR_CODES";

$extra['functions'] = [
	'URL_OR_CODES' => 'NFCQRActionsMakeURLCodes',
];

if ( User( 'PROFILE' ) === 'parent' )
{
	$staff_RET = DBGet( "SELECT " . DisplayNameSQL( 's' ) . " AS FULL_NAME,
		s.PROFILE,s.PROFILE_ID,s.STAFF_ID,s.SCHOOLS " . $extra['SELECT'] .
		" FROM staff s
		WHERE STAFF_ID='" . User( 'STAFF_ID' ) . "'",
		$extra['functions'] + [ 'PROFILE' => 'makeProfile' ] );

	$columns = [
		'FULL_NAME' => _( 'User' ),
		'PROFILE' => _( 'Profile' ),
		'STAFF_ID' => sprintf( _( '%s ID' ), Config( 'NAME' ) ),
	] + $extra['columns_after'];

	ListOutput(
		$staff_RET,
		$columns,
		'User',
		'Users'
	);
}
else
{
	$extra['link'] = [ 'FULL_NAME' => false ];

	$extra['new'] = true;

	Search( 'staff_id', $extra );
}

if ( ! $_REQUEST['url_or_codes'] )
{
	NFCQRActionsCopyURLJS();
}
