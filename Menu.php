<?php
/**
 * Menu.php file
 * Required
 * - Menu entries for the NFC/QR Actions module
 *
 * @package NFC/QR Actions module
 */

$menu['NFC_QR_Actions']['admin'] = [ // Admin menu.
	'title' => dgettext( 'NFC_QR_Actions', 'NFC/QR Actions' ),
	'default' => 'NFC_QR_Actions/MyActions.php', // Program loaded by default when menu opened.
	'NFC_QR_Actions/MyActions.php' => dgettext( 'NFC_QR_Actions', 'My Actions' ),
	1 => _( 'Setup' ),
	'NFC_QR_Actions/Actions.php' => dgettext( 'NFC_QR_Actions', 'Actions' ),
	'NFC_QR_Actions/URLCodes.php' => dgettext( 'NFC_QR_Actions', 'NFC URL/QR codes' ),
] + issetVal( $menu['NFC_QR_Actions']['admin'], [] );

$menu['NFC_QR_Actions']['teacher'] = [ // Teacher menu.
	'title' => dgettext( 'NFC_QR_Actions', 'NFC/QR Actions' ),
	'default' => 'NFC_QR_Actions/MyActions.php', // Program loaded by default when menu opened.
	'NFC_QR_Actions/MyActions.php' => dgettext( 'NFC_QR_Actions', 'My Actions' ),
	1 => _( 'Setup' ),
	'NFC_QR_Actions/URLCodes.php' => dgettext( 'NFC_QR_Actions', 'NFC URL/QR codes' ),
] + issetVal( $menu['NFC_QR_Actions']['teacher'], [] );

$menu['NFC_QR_Actions']['parent'] = [ // Parent & Student menu.
	'title' => dgettext( 'NFC_QR_Actions', 'NFC/QR Actions' ),
	'default' => 'NFC_QR_Actions/URLCodes.php', // Program loaded by default when menu opened.
	'NFC_QR_Actions/URLCodes.php' => dgettext( 'NFC_QR_Actions', 'NFC URL/QR codes' ),
] + issetVal( $menu['NFC_QR_Actions']['parent'], [] );
