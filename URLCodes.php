<?php
/**
 * NFC URL/QR codes
 *
 * @package NFC/QR Actions module
 */

require_once 'modules/NFC_QR_Actions/includes/URLCodes.fnc.php';

if ( empty( $_SESSION['NFC_QR_Actions_type'] ) )
{
	$_SESSION['NFC_QR_Actions_type'] = 'student';
}

if ( ! empty( $_REQUEST['type'] ) )
{
	$_SESSION['NFC_QR_Actions_type'] = $_REQUEST['type'];
}
else
{
	$_REQUEST['type'] = $_SESSION['NFC_QR_Actions_type'];
}

$header = '<a href="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&type=student' ) . '">' .
	( $_REQUEST['type'] === 'student' ?
	'<b>' . _( 'Students' ) . '</b>' : _( 'Students' ) ) . '</a>';

$header .= ' | <a href="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&type=staff' ) . '">' .
	( $_REQUEST['type'] === 'staff' ?
	'<b>' . _( 'Users' ) . '</b>' : _( 'Users' ) ) . '</a>';

DrawHeader(  ( $_REQUEST['type'] === 'staff' ? _( 'User' ) : _( 'Student' ) ) . ' &minus; ' . ProgramTitle() );
User( 'PROFILE' ) === 'student' ? '' : DrawHeader( $header );

require_once 'modules/NFC_QR_Actions/' . ( $_REQUEST['type'] == 'staff' ? 'Users' : 'Students' ) . '/URLCodes.php';
