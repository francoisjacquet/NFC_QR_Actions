<?php
/**
 * Actions functions
 *
 * @package Dashboards module
 */

/**
 * Make Text Input
 *
 * DBGet() callback
 *
 * @param string $value
 * @param string $column
 *
 * @return string Text Input
 */
function NFCQRActionsMakeTextInput( $value, $column = 'TITLE' )
{
	global $THIS_RET;

	$id = ! empty( $THIS_RET['ID'] ) ? $THIS_RET['ID'] : 'new';

	if ( $column === 'URL_STUDENT'
		|| $column === 'URL_STAFF' )
	{
		$extra = 'size="32" maxlength="1000" autocomplete="off"';
	}

	if ( $column === 'TITLE' )
	{
		$extra = 'maxlength="30"';
	}

	if ( $id !== 'new'
		&& $column === 'TITLE' )
	{
		$extra .= ' required';
	}

	return TextInput( $value, 'values[' . $id . '][' . $column . ']', '', $extra );
}

/**
 * Make URL
 *
 * DBGet() callback
 *
 * @uses NFCQRActionsMakeTextInput()
 *
 * @param string $value
 * @param string $name
 *
 * @return string URL input HTML
 */
function NFCQRActionsMakeURL( $value, $name = 'URL_STUDENT' )
{
	global $THIS_RET;

	if ( ! empty( $_REQUEST['LO_save'] ) )
	{
		// Export list.
		return $value;
	}

	if ( $value )
	{
		// Truncate links > 90 chars.
		$truncated_link = $value;

		if ( mb_strlen( $truncated_link ) > 90 )
		{
			$separator = '/.../';
			$separator_length = mb_strlen( $separator );
			$max_length = 90 - $separator_length;
			$start = (int) ( $max_length / 2 );
			$trunc = mb_strlen( $truncated_link ) - $max_length;
			$truncated_link = substr_replace( $truncated_link, $separator, $start, $trunc );
		}

		// Add `&nfc_qr_action=ID` to URL
		$link = $value . ( mb_strpos( $value, '?' ) !== false ? '&' : '?' ) .
			'nfc_qr_action=' . $THIS_RET['ID'];

		return '<div style="display:table-cell;"><a href="' . URLEscape( $link ) . '" target="_blank">' .
			_( 'Link' ) . '</a>&nbsp;</div>
			<div style="display:table-cell;">' . NFCQRActionsMakeTextInput( [ $value, $truncated_link ], $name ) . '</div>';
	}

	return NFCQRActionsMakeTextInput( $value, $name );
}


/**
 * Make Configuration link
 *
 * DBGet() callback
 *
 * @param string $value
 * @param string $column
 *
 * @return string Configuration link
 */
function NFCQRActionsMakeConfiguration( $value, $column = 'CONFIGURATION' )
{
	global $THIS_RET;

	if ( empty( $THIS_RET['URL_STUDENT'] )
		&& empty( $THIS_RET['URL_STAFF'] ) )
	{
		return '';
	}

	$link = 'Modules.php?modname=' . $_REQUEST['modname'] . '&id=' . $value;

	return '<a href="' . URLEscape( $link ) . '">' . _( 'Configuration' ) . '</a>';
}

/**
 * Display Template Checkbox
 *
 * @param string $value Input value.
 * @param string $name  Input name.
 *
 * @return string Checkbox Input.
 */
function NFCQRActionsDisplayTemplateCheckbox( $value, $name )
{
	$title = dgettext( 'NFC_QR_Actions', 'Perform action and display student information' );

	if ( mb_strpos( $name, 'staff' ) !== false )
	{
		$title = dgettext( 'NFC_QR_Actions', 'Perform action and display user information' );
	}

	return CheckboxInput(
		$value,
		$name,
		$title,
		'',
		true
	);
}

/**
 * Template TinyMCE Input + Substitutions
 *
 * @param string $value Input value.
 * @param string $name  Input name.
 *
 * @return string TinyMCE Input + Substitutions.
 */
function NFCQRActionsTemplateInput( $value, $name )
{
	global $RosarioModules;

	$input = TinyMCEInput(
		$value,
		$name,
		dgettext( 'NFC_QR_Actions', 'Template' )
	);

	$substitutions = [
		'__FULL_NAME__' => _( 'Display Name' ),
		'__LAST_NAME__' => _( 'Last Name' ),
		'__FIRST_NAME__' => _( 'First Name' ),
		'__MIDDLE_NAME__' =>  _( 'Middle Name' ),
		'__USERNAME__' => _( 'Username' ),
	];

	if ( mb_strpos( $name, 'STAFF' ) !== false )
	{
		$substitutions += [
			'__STAFF_ID__' => _( 'User ID' ),
			'__PROFILE__' => _( 'Profile' ),
			'__PHOTO__' => _( 'User Photo' ),
		];

		$substitutions += SubstitutionsCustomFields( 'staff' );
	}
	else
	{
		$substitutions += [
			'__STUDENT_ID__' => sprintf( _( '%s ID' ), Config( 'NAME' ) ),
			'__SCHOOL_TITLE__' => _( 'School' ),
			'__GRADE_ID__' => _( 'Grade Level' ),
			'__PHOTO__' => _( 'Student Photo' ),
			'__CONTACT_INFO__' => _( 'Contact Information' ),
			'__ATTENDANCE_TEACHER__' => _( 'Attendance Teacher' ),
		];

		if ( ! empty( $RosarioModules['Hostel'] ) )
		{
			$substitutions += [ '__HOSTEL_ROOM__' => dgettext( 'Hostel', 'Room' ) ];
		}

		if ( ! empty( $RosarioModules['Entry_Exit'] ) )
		{
			$substitutions += [
				'__ENTRY_EXIT_HAS_PACKAGE_TO_PICKUP__' => dgettext( 'Entry_Exit', 'Has a package to pickup' ),
				'__ENTRY_EXIT_PRIVATE_NOTES__' => dgettext( 'Entry_Exit', 'Private Notes' ),
			];
		}

		$substitutions += SubstitutionsCustomFields( 'student' );
	}

	$substitutions_input = SubstitutionsInput( $substitutions );

	return $input . '<br />' . $substitutions_input;
}
