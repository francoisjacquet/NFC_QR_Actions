<?php
/**
 * NFC URL/QR codes functions
 *
 * @package NFC/QR Actions
 */

function NFCQRActionsURLCodesSelect()
{
	$url_or_codes_options = [
		'' => dgettext( 'NFC_QR_Actions', 'NFC URL' ),
		'qr' => dgettext( 'NFC_QR_Actions', 'QR codes' ),
	];

	return SelectInput(
		$_REQUEST['url_or_codes'],
		'url_or_codes',
		'<span class="a11y-hidden">' . dgettext( 'NFC_QR_Actions', 'NFC URL or QR codes' ) . '</span>',
		$url_or_codes_options,
		false,
		( version_compare( ROSARIO_VERSION, '12.5', '>=' ) ?
			// @since RosarioSIS 12.5 CSP remove unsafe-inline Javascript
			'class="onchange-ajax-post-form"' :
			'onchange="ajaxPostForm(this.form, true);"' ),
		false
	);
}

function NFCQRActionsDownloadQRCodesButton()
{
	ob_start();
	// Load JS to save images as zip.
	?>
	<script async defer src="modules/NFC_QR_Actions/js/jszip.min.js"></script>
	<script async defer src="modules/NFC_QR_Actions/js/FileSaver.min.js"></script>
	<script async defer src="modules/NFC_QR_Actions/js/NFCQRActionsDownloadQRCodesButton.js?v=1.7"></script>
	<input type="button" id="download-zip-button"
		value="<?php echo AttrEscape( dgettext( 'NFC_QR_Actions', 'Download QR codes as zip' ) ); ?>">
	<?php
	return ob_get_clean();
}

/**
 * Make NFC URL or QR codes
 *
 * Hash: base64_encode(STUDENT_ID||CREATED_AT)
 * Security so URL cannot be forged.
 *
 * @uses modules/NFC_QR_Actions/qrcode_image.php script for QR codes
 *
 * @param int $value     CREATED_AT column.
 * @param string $column URL_OR_CODES.
 *
 * @return string NFC URL (copy), or QR code.
 */
function NFCQRActionsMakeURLCodes( $value, $column = 'URL_OR_CODES' )
{
	global $THIS_RET;

	if ( ( empty( $THIS_RET['STUDENT_ID'] ) && empty( $THIS_RET['STAFF_ID'] ) )
		|| empty( $_REQUEST['type'] ) )
	{
		return '';
	}

	if ( ! $value )
	{
		// Fix user/student hash when CREATED_AT is NULL (RosarioSIS < 12.1 + MySQL)
		$value = date( 'Y-m-d H:i:s', 0 );
	}

	$user_id = ! empty( $THIS_RET['STUDENT_ID'] ) ? $THIS_RET['STUDENT_ID'] : $THIS_RET['STAFF_ID'];

	$site_url = RosarioURL();

	// Hash: base64_encode(STUDENT_ID||CREATED_AT)
	// Security so URL cannot be forged.
	$hash = base64_encode( $user_id . '||' . $value );

	$url = $site_url . 'Modules.php?modname=NFC_QR_Actions/MyActions.php&modfunc=do_action&h=' . $hash .
		'&type=' . $_REQUEST['type'] . '&id=' . $user_id; // User ID is just here for readability.

	if ( ! $_REQUEST['url_or_codes'] )
	{
		if ( ! empty( $_REQUEST['LO_save'] ) )
		{
			return $url;
		}

		// NFC URL.
		// Copy URL onclick, see JS in NFCQRActionsCopyURLJS().
		$link = '<a href="' . URLEscape( $url ) . '" class="url-copy" target="_blank">' . _( 'Copy' ) . '</a>';

		return $link;
	}

	// QR codes + add User ID below.
	$qr_code_image_url = $site_url . 'modules/NFC_QR_Actions/qrcode_image.php?url=' . urlencode( $url ) .
		'&label=' . $user_id;

	$image_id = $_REQUEST['type'] . '-' . $user_id;

	$image = '<img src="' . URLEscape( $qr_code_image_url ) . '" class="qr-code-img" id="' . AttrEscape( $image_id ) . '" width="150" />';

	// Fix responsive rt td too large.
	return '<div id="divNfcQrActionsCode' . $user_id . '" class="rt2colorBox">' .
		$image . '</div>';
}

/**
 * JS to copy URL to clipboard
 *
 * @link https://stackoverflow.com/questions/53369140/copy-link-of-an-anchor-tag-to-clipboard-when-clicking-on-it#53369306
 */
function NFCQRActionsCopyURLJS()
{
	?>
	<input type="hidden" disabled id="url_copied_text"
		value="<?php echo AttrEscape( dgettext( 'NFC_QR_Actions', 'Copied' ) ); ?>" />
	<script src="modules/NFC_QR_Actions/js/NFCQRActionsCopyURL.js?v=1.7"></script>
	<?php
}
