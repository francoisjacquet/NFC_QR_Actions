<?php
/**
 * My Actions functions
 *
 * @package Dashboards module
 */

/**
 * Make Number Input
 *
 * DBGet() callback
 *
 * @param string $value
 * @param string $column
 *
 * @return string Number Input
 */
function NFCQRActionsMakeNumberInput( $value, $column = 'CLOSE_TAB_AFTER_SECONDS' )
{
	global $THIS_RET;

	$id = ! empty( $THIS_RET['ID'] ) ? $THIS_RET['ID'] : 'new';

	$extra = 'type="number" min="1" max="999"';

	// No input if URL are external AND no Template.
	if ( $id !== 'new' )
	{
		$action_id = $THIS_RET['ACTION_ID'];

		$url_student = DBGetOne( "SELECT URL_STUDENT
			FROM nfc_qr_actions
			WHERE ID='" . (int) $action_id . "'
			AND STUDENT_TEMPLATE IS NULL" );

		if ( $url_student
			&& mb_strpos( $url_student, 'Modules.php?modname=' ) !== 0 )
		{
			return button( 'warning' ) . '&nbsp;' . dgettext( 'NFC_QR_Actions', 'External URL' );
		}

		$url_staff = DBGetOne( "SELECT URL_STAFF
			FROM nfc_qr_actions
			WHERE ID='" . (int) $action_id . "'
			AND STAFF_TEMPLATE IS NULL" );

		if ( $url_staff
			&& mb_strpos( $url_staff, 'Modules.php?modname=' ) !== 0 )
		{
			return button( 'warning' ) . '&nbsp;' . dgettext( 'NFC_QR_Actions', 'External URL' );
		}
	}

	return TextInput( $value, 'values[' . $id . '][' . $column . ']', '', $extra );
}

/**
 * Make Action Select input
 * Teacher: limit actions to AllowUse() programs
 *
 * DBGet() callback
 *
 * @param string $value
 * @param string $name
 *
 * @return string Select input HTML
 */
function NFCQRActionsMakeAction( $value, $column = 'ACTION_ID' )
{
	global $THIS_RET;

	$id = ! empty( $THIS_RET['ID'] ) ? $THIS_RET['ID'] : 'new';

	$actions_RET = DBGet( "SELECT ID,TITLE,URL_STUDENT,URL_STAFF
		FROM nfc_qr_actions" );

	$options = [];

	foreach ( (array) $actions_RET as $action )
	{
		if ( User( 'PROFILE' ) === 'teacher' )
		{
			foreach ( [ $action['URL_STUDENT'], $action['URL_STAFF'] ] as $action_url )
			{
				// Teacher: limit actions to AllowUse() programs.
				if ( ! $action_url
					|| mb_strpos( $action_url, 'Modules.php?modname=' ) !== 0 )
				{
					continue;
				}

				$modname_pos = mb_strpos( $action_url, '?modname=' ) + 9;

				// Extract modname from URL.
				$action_modname = mb_substr(
					$action_url,
					$modname_pos,
					mb_strpos( $action_url, '&' ) - $modname_pos
				);

				if ( ! AllowUse( $action_modname ) )
				{
					continue 2;
				}
			}
		}

		$options[ $action['ID'] ] = $action['TITLE'];
	}

	return SelectInput(
		$value,
		'values[' . $id . '][' . $column . ']',
		'',
		$options,
		( $id === 'new' ? 'N/A' : false )
	);
}

/**
 * Get User ID (student or staff) from Hash (in URL)
 * Hash = base64_encode(STUDENT_ID||CREATED_AT)
 * Check type 'student' or 'staff'
 * Check hash is well formed
 * Check current user can access student/user
 * Check that CREATED_AT matches
 *
 * @param string $type 'student' or 'staff'.
 * @param string $hash Base64 hash from URL.
 *
 * @global $error Add specific error for debug purpose if ROSARIO_DEBUG
 *
 * @return int User ID (student or staff) or -1 if Invalid URL or 0 if No Students/Users were found.
 */
function NFCQRActionsGetUserIDFromHash( $type, $hash )
{
	global $error;

	// Check hash, type.
	if ( empty( $type )
		|| empty( $hash )
		|| ! in_array( $type, [ 'student', 'staff' ] ) )
	{
		if ( ROSARIO_DEBUG )
		{
			$error[] = 'Invalid type or empty hash';
		}

		return -1;
	}

	// Check Hash: base64_encode(STUDENT_ID||CREATED_AT)
	$hash = base64_decode( $hash, true );

	if ( ! $hash )
	{
		if ( ROSARIO_DEBUG )
		{
			$error[] = 'Error when decoding base64 hash';
		}

		return -1;
	}

	list( $user_id, $created_at ) = explode( '||', $hash );

	if ( ! $user_id
		|| ! $created_at )
	{
		if ( ROSARIO_DEBUG )
		{
			$error[] = 'No user ID or CREATED_AT found in decoded hash';
		}

		return -1;
	}

	// Search all schools.
	$_REQUEST['_search_all_schools'] = 'Y';

	// Check User ID.
	if ( $type === 'student' )
	{
		$extra['SELECT_ONLY'] = "s.CREATED_AT";

		$extra['WHERE'] = " AND s.STUDENT_ID='" . (int) $user_id . "'";

		$user_RET = GetStuList( $extra );
	}
	elseif ( $type === 'staff' )
	{
		$extra['SELECT'] = ",s.CREATED_AT";

		$extra['WHERE'] = " AND s.STAFF_ID='" . (int) $user_id . "'";

		$user_RET = GetStaffList( $extra );
	}

	if ( empty( $user_RET[1]['CREATED_AT'] ) )
	{
		// Fix user/student hash when CREATED_AT is NULL (RosarioSIS < 12.1 + MySQL)
		$user_RET[1]['CREATED_AT'] = date( 'Y-m-d H:i:s', 0 );
	}

	if ( $user_RET[1]['CREATED_AT'] !== $created_at )
	{
		if ( ROSARIO_DEBUG )
		{
			$error[] = 'User found but CREATED_AT does not match';
		}

		// User found but CREATED_AT does not match. Typically an HackingAttempt.
		return -1;
	}

	return $user_id;
}

/**
 * Get My Action
 *
 * @uses $_SESSION['NFC_QR_Default_Action']
 * @uses $_REQUEST['action_id']
 * @uses $_REQUEST['set_default']
 *
 * @param string $type 'student' or 'staff'.
 *
 * @return int Action ID, or 0 if no actions, or -1 if various actions found & no default / selected.
 */
function NFCQRActionsGetMyAction( $type )
{
	$actions = NFCQRActionsGetMyPossibleActions( $type );

	$default_action = issetVal( $_SESSION['NFC_QR_Default_Action'] );

	$selected_action = issetVal( $_REQUEST['action_id'] );

	if ( $selected_action
		&& ! empty( $_REQUEST['set_default'] ) )
	{
		// Set default action.
		$_SESSION['NFC_QR_Default_Action'] = $selected_action;
	}

	if ( ! $actions )
	{
		return 0;
	}

	if ( count( $actions ) === 1
		|| ( $default_action && in_array( $default_action, $actions ) )
		|| ( $selected_action && in_array( $selected_action, $actions ) ) )
	{
		if ( count( $actions ) === 1 )
		{
			return reset( $actions );
		}

		if ( $default_action && in_array( $default_action, $actions ) )
		{
			return $default_action;
		}

		return $selected_action;
	}

	return -1;
}

/**
 * Get My Possible Actions
 *
 * @param string $type 'student' or 'staff'.
 *
 * @return array Possible Action IDs.
 */
function NFCQRActionsGetMyPossibleActions( $type )
{
	if ( $type === 'student' )
	{
		$actions_RET = DBGet( "SELECT nqa.ID
			FROM nfc_qr_actions nqa,nfc_qr_staff_actions nqsa
			WHERE nqa.ID=nqsa.ACTION_ID
			AND nqsa.STAFF_ID='" . User( 'STAFF_ID' ) . "'
			AND nqa.URL_STUDENT IS NOT NULL" );
	}
	elseif ( $type === 'staff' )
	{
		$actions_RET = DBGet( "SELECT nqa.ID
			FROM nfc_qr_actions nqa,nfc_qr_staff_actions nqsa
			WHERE nqa.ID=nqsa.ACTION_ID
			AND nqsa.STAFF_ID='" . User( 'STAFF_ID' ) . "'
			AND nqa.URL_STAFF IS NOT NULL" );
	}

	$actions = [];

	foreach ( (array) $actions_RET as $action )
	{
		$actions[] = $action['ID'];
	}

	return $actions;
}

/**
 * My Possible Actions Select input
 *
 * @param string $type 'student' or 'staff'.
 *
 * @return string Select input HTML
 */
function NFCQRActionsMySelect( $type )
{
	$actions = NFCQRActionsGetMyPossibleActions( $type );

	$actions_list = implode( "','", $actions );

	$actions_RET = DBGet( "SELECT ID,TITLE
		FROM nfc_qr_actions
		WHERE ID IN('" . ( $actions_list ? $actions_list : '0' ) . "')" );

	$options = [];

	foreach ( (array) $actions_RET as $action )
	{
		$options[ $action['ID'] ] = $action['TITLE'];
	}

	return SelectInput(
		'',
		'action_id',
		'<span class="a11y-hidden">' . dgettext( 'NFC_QR_Actions', 'My Actions' ) . '</span>',
		$options,
		false,
		'required'
	);
}

/**
 * Get Action URL
 * Replace __STUDENT_ID__ or __STAFF_ID__ with User ID
 *
 * @param string $type      'student' or 'staff'.
 * @param int    $action_id Action ID.
 * @param int    $user_id   User ID.
 *
 * @return string Action URL.
 */
function NFCQRActionsGetActionURL( $type, $action_id, $user_id )
{
	if ( $type === 'student' )
	{
		// Get action URL.
		$action_url = (string) DBGetOne( "SELECT URL_STUDENT
			FROM nfc_qr_actions
			WHERE ID='" . (int) $action_id . "'" );

		$action_url = str_replace( '__STUDENT_ID__', $user_id, $action_url );
	}
	elseif ( $type === 'staff' )
	{
		// Get action URL.
		$action_url = (string) DBGetOne( "SELECT URL_STAFF
			FROM nfc_qr_actions
			WHERE ID='" . (int) $action_id . "'" );

		$action_url = str_replace( '__STAFF_ID__', $user_id, $action_url );
	}

	// Add `&nfc_qr_action=ID` to URL
	$action_url .= ( mb_strpos( $action_url, '?' ) !== false ? '&' : '?' ) .
		'nfc_qr_action=' . $action_id;

	return $action_url;
}

/**
 * Action has Template?
 *
 * @param string $type      'student' or 'staff'.
 * @param int    $action_id Action ID.
 *
 * @return bool True if Action has template.
 */
function NFCQRActionsActionHasTemplate( $type, $action_id )
{
	if ( $type === 'student' )
	{
		return (bool) DBGetOne( "SELECT 1
			FROM nfc_qr_actions
			WHERE ID='" . (int) $action_id . "'
			AND STUDENT_TEMPLATE IS NOT NULL" );
	}

	return (bool) DBGetOne( "SELECT 1
		FROM nfc_qr_actions
		WHERE ID='" . (int) $action_id . "'
		AND STAFF_TEMPLATE IS NOT NULL" );
}


/**
 * Get Action Template
 *
 * @param string $type      'student' or 'staff'.
 * @param int    $action_id Action ID.
 *
 * @return string Action template.
 */
function NFCQRActionsGetActionTemplate( $type, $action_id, $user_id )
{
	global $RosarioModules;

	// Search all schools.
	$_REQUEST['_search_all_schools'] = 'Y';

	if ( $type === 'student' )
	{
		// Get action Template.
		$action_template = DBGetOne( "SELECT STUDENT_TEMPLATE
			FROM nfc_qr_actions
			WHERE ID='" . (int) $action_id . "'" );

		if ( ! $action_template )
		{
			return '';
		}

		$extra['WHERE'] = " AND s.STUDENT_ID='" . (int) $user_id . "'";

		// SELECT s.* Custom Fields for Substitutions.
		$extra['SELECT'] = ",s.*";

		if ( mb_strpos( $action_template, '__CONTACT_INFO__' ) !== false )
		{
			$_REQUEST['expanded_view'] = 'true';
		}

		if ( mb_strpos( $action_template, '__ATTENDANCE_TEACHER__' ) !== false )
		{
			$extra['SELECT'] .= ",(SELECT " . DisplayNameSQL( 'st' ) . "
			FROM staff st,course_periods cp,school_periods p,schedule ss,course_period_school_periods cpsp
			WHERE st.STAFF_ID=cp.TEACHER_ID
			AND cpsp.PERIOD_id=p.PERIOD_ID
			AND cp.DOES_ATTENDANCE IS NOT NULL
			AND cpsp.COURSE_PERIOD_ID=cp.COURSE_PERIOD_ID
			AND cp.COURSE_PERIOD_ID=ss.COURSE_PERIOD_ID
			AND ss.STUDENT_ID=s.STUDENT_ID
			AND ss.SYEAR='" . UserSyear() . "'
			AND ss.MARKING_PERIOD_ID IN (" . GetAllMP( 'QTR', GetCurrentMP( 'QTR', DBDate(), false ) ) . ")
			AND (ss.START_DATE<='" . DBDate() . "'
				AND (ss.END_DATE>='" . DBDate() . "' OR ss.END_DATE IS NULL))
			ORDER BY p.SORT_ORDER IS NULL,p.SORT_ORDER LIMIT 1) AS ATTENDANCE_TEACHER";
		}

		if ( ! empty( $RosarioModules['Hostel'] )
			&& mb_strpos( $action_template, '__HOSTEL_ROOM__' ) !== false )
		{
			// Room.
			$extra['SELECT'] .= ",(SELECT CONCAT((SELECT TITLE
					FROM hostel_buildings
					WHERE ID=hr.BUILDING_ID), ' - ', hr.TITLE)
				FROM hostel_rooms hr,hostel_students hs
				WHERE s.STUDENT_ID=hs.STUDENT_ID
				AND hr.ID=hs.ROOM_ID
				LIMIT 1) AS HOSTEL_ROOM";
		}

		if ( ! empty( $RosarioModules['Entry_Exit'] ) )
		{
			if ( mb_strpos( $action_template, '__ENTRY_EXIT_PRIVATE_NOTES__' ) !== false )
			{
				// Private Notes.
				$extra['SELECT'] .= ",(SELECT VALUE
					FROM program_user_config
					WHERE s.STUDENT_ID=(USER_ID *-1)
					AND PROGRAM='EntryExitStudent'
					AND TITLE='PRIVATE_NOTES'
					LIMIT 1) AS ENTRY_EXIT_PRIVATE_NOTES";
			}

			if ( mb_strpos( $action_template, '__ENTRY_EXIT_HAS_PACKAGE_TO_PICKUP__' ) !== false )
			{
				// Has a package to pickup.
				$extra['SELECT'] .= ",(SELECT VALUE
					FROM program_user_config
					WHERE s.STUDENT_ID=(USER_ID *-1)
					AND PROGRAM='EntryExitStudent'
					AND TITLE='HAS_PACKAGE_TO_PICKUP'
					LIMIT 1) AS ENTRY_EXIT_HAS_PACKAGE_TO_PICKUP";
			}
		}

		$RET = GetStuList( $extra );

		if ( ! empty( $RET[1] ) )
		{
			$student = $RET[1];

			$action_template = NFCQRActionsTemplateMakeSubstitutions( $type, $action_template, $student );
		}
	}
	elseif ( $type === 'staff' )
	{
		// Get action Template.
		$action_template = DBGetOne( "SELECT STAFF_TEMPLATE
			FROM nfc_qr_actions
			WHERE ID='" . (int) $action_id . "'" );

		if ( ! $action_template )
		{
			return '';
		}

		$extra['WHERE'] = " AND s.STAFF_ID='" . (int) $user_id . "'";

		// SELECT s.* Custom Fields for Substitutions.
		$extra['SELECT'] = ",s.*";

		$RET = GetStaffList( $extra );

		if ( ! empty( $RET[1] ) )
		{
			$user = $RET[1];

			$action_template = NFCQRActionsTemplateMakeSubstitutions( $type, $action_template, $user );
		}
	}

	return (string) $action_template;
}

/**
 * Action Template Make Substitutions
 *
 * @param string $type     'student' or 'staff'.
 * @param string $template Action Template.
 * @param string $user     User (staff or student) Info array.
 *
 * @return string Substituted Action Template.
 */
function NFCQRActionsTemplateMakeSubstitutions( $type, $template, $user )
{
	global $StudentPicturesPath,
		$UserPicturesPath;

	$user_id = $type === 'student' ? $user['STUDENT_ID'] : $user['STAFF_ID'];

	$user_photo = '';

	if ( $type === 'student' )
	{
		$picture_path = (array) glob( $StudentPicturesPath . '*/' . (int) $user_id . '.*jpg' );

		$picture_path = end( $picture_path );

		if ( $picture_path )
		{
			$user_photo = '<img src="' . URLEscape( $picture_path ) . '" class="user-photo" alt="' . AttrEscape( _( 'Student Photo' ) ) . '" />';
		}
	}
	else
	{
		$picture_path = (array) glob( $UserPicturesPath . UserSyear() . '/' . (int) $user_id . '.*jpg' );

		$picture_path = end( $picture_path );

		if ( ! $picture_path
			&& ! empty( $user['ROLLOVER_ID'] ) )
		{
			// Use Last Year's if Missing.
			$picture_path = (array) glob( $UserPicturesPath . ( UserSyear() - 1 ) . '/' . $user['ROLLOVER_ID'] . '.*jpg' );

			$picture_path = end( $picture_path );
		}

		if ( $picture_path )
		{
			$user_photo = '<img src="' . URLEscape( $picture_path ) . '" class="user-photo" alt="' . AttrEscape( _( 'User Photo' ) ) . '" />';
		}
	}

	$substitutions = [
		'__FULL_NAME__' => $user['FULL_NAME'],
		'__LAST_NAME__' => $user['LAST_NAME'],
		'__FIRST_NAME__' => $user['FIRST_NAME'],
		'__MIDDLE_NAME__' =>  $user['MIDDLE_NAME'],
		'__USERNAME__' => $user['USERNAME'],
		'__PHOTO__' => $user_photo,
	];

	if ( $type === 'student' )
	{
		$substitutions += [
			'__STUDENT_ID__' => $user_id,
			'__SCHOOL_TITLE__' => $user['SCHOOL_TITLE'],
			'__GRADE_ID__' => $user['GRADE_ID'],
			'__CONTACT_INFO__' => issetVal( $user['CONTACT_INFO'], '' ),
			'__ATTENDANCE_TEACHER__' => issetVal( $user['ATTENDANCE_TEACHER'], '' ),
		];

		if ( array_key_exists( 'HOSTEL_ROOM', $user ) )
		{
			$substitutions += [ '__HOSTEL_ROOM__' => $user['HOSTEL_ROOM'] ];
		}

		if ( array_key_exists( 'ENTRY_EXIT_HAS_PACKAGE_TO_PICKUP', $user ) )
		{
			$substitutions += [
				'__ENTRY_EXIT_HAS_PACKAGE_TO_PICKUP__' => $user['ENTRY_EXIT_HAS_PACKAGE_TO_PICKUP'] ?
					_( 'Yes' ) : _( 'No' )
			];
		}

		if ( array_key_exists( 'ENTRY_EXIT_PRIVATE_NOTES', $user ) )
		{
			$substitutions += [ '__ENTRY_EXIT_PRIVATE_NOTES__' => $user['ENTRY_EXIT_PRIVATE_NOTES'] ];
		}
	}
	else
	{
		$substitutions += [
			'__STAFF_ID__' => $user_id,
			'__PROFILE__' => $user['PROFILE'],
		];
	}

	$substitutions += SubstitutionsCustomFieldsValues( $type, $user );

	return SubstitutionsTextMake( $substitutions, $template );
}

/**
 * Get My Action Close Tab After (seconds)
 *
 * @param int    $action_id Action ID.
 *
 * @return int Close Tab After (seconds).
 */
function NFCQRActionsGetMyActionCloseTabAfterSeconds( $action_id )
{
	// Get My Action Close Tab After (seconds).
	return (int) DBGetOne( "SELECT CLOSE_TAB_AFTER_SECONDS
		FROM nfc_qr_staff_actions
		WHERE ACTION_ID='" . (int) $action_id . "'
		AND STAFF_ID='" . User( 'STAFF_ID' ) . "'" );
}

function NFCQRActionsCloseTabAfterSecondsJS( $seconds )
{
	ob_start();
	?>
	<input type="hidden" disabled id="close_tab_after_seconds"
		value="<?php echo AttrEscape( (int) $seconds ); ?>" />
	<script src="modules/NFC_QR_Actions/js/NFCQRActionsCloseTabAfterSeconds.js?v=1.7"></script>
	<?php

	return ob_get_clean();
}
