<?php
/**
 * Do Action
 *
 * @package NFC QR Actions module
 */

$user_id = NFCQRActionsGetUserIDFromHash(
	issetVal( $_REQUEST['type'], '' ),
	issetVal( $_REQUEST['h'], '' )
);

if ( ! $user_id )
{
	// Error message if no student/user found (GetStuList/GetStaffList SELECT_ONLY CREATED_AT)
	$error[] = $_REQUEST['type'] === 'student' ?
		_( 'No Students were found.' ) : _( 'No Users were found.' );

	return;
}

if ( $user_id == -1 )
{
	$error[] = dgettext( 'NFC_QR_Actions', 'Invalid URL' );

	return;
}

$action_id = NFCQRActionsGetMyAction( $_REQUEST['type'] );

if ( ! $action_id )
{
	$error[] = dgettext( 'NFC_QR_Actions', 'No possible actions were found.' );

	return;
}

if ( $action_id == -1 )
{
	echo '<form action="' . PreparePHP_SELF() . '" method="GET">';

	if ( ! AllowEdit() )
	{
		$_ROSARIO['allow_edit'] = true;

		$allow_edit_tmp = true;
	}

	// Select action & set as default action for this session.
	PopTable( 'header', dgettext( 'NFC_QR_Actions', 'Select an action' ) );

	echo NFCQRActionsMySelect( $_REQUEST['type'] );

	echo '<br />';

	echo CheckboxInput(
		'',
		'set_default',
		dgettext( 'NFC_QR_Actions', 'Set as default action for this session' ),
		'',
		true
	);

	echo '<br /><br /><div class="center">' . SubmitButton( _( 'Select' ) ) . '</div>';

	PopTable( 'footer' );

	if ( ! empty( $allow_edit_tmp ) )
	{
		$_ROSARIO['allow_edit'] = false;
	}

	echo '</form>';

	return;
}

// Redirect to Action URL, or do Action in background & display Student / User Info.
$action_url = NFCQRActionsGetActionURL( $_REQUEST['type'], $action_id, $user_id );

if ( ! $action_url )
{
	if ( $_REQUEST['type'] === 'student' )
	{
		$error[] = dgettext( 'NFC_QR_Actions', 'No URL (Student) set for this action.' );
	}
	else
	{
		$error[] = dgettext( 'NFC_QR_Actions', 'No URL (User) set for this action.' );
	}

	return;
}

$close_tab_after_seconds = NFCQRActionsGetMyActionCloseTabAfterSeconds( $action_id );

if ( $close_tab_after_seconds )
{
	echo NFCQRActionsCloseTabAfterSecondsJS( $close_tab_after_seconds );
}

$has_template = NFCQRActionsActionHasTemplate( $_REQUEST['type'], $action_id );

// Note: hidden input values are used by JS
?>
<input type="hidden" disabled id="nfc_qr_action_url"
	value="<?php echo URLEscape( $action_url ); ?>" />
<input type="hidden" disabled id="nfc_qr_action_has_template"
	value="<?php echo AttrEscape( $has_template ? 'Y' : '' ); ?>" />
<script src="modules/NFC_QR_Actions/js/DoAction.inc.js?v=1.7"></script>
<?php

if ( $has_template )
{
	if ( empty( $_REQUEST['action_done'] ) )
	{
		$_REQUEST['action_done'] = 'Y';
	}

	if ( $close_tab_after_seconds )
	{
		// Link to cancel "Close tab after (seconds)".
		DrawHeader(
			'<a href="#!" id="cancel_close_tab">' .
				dgettext( 'NFC_QR_Actions', 'Cancel automatic tab closing' ) . '</a>'
		);

		echo '<br />';
	}

	$action_template = NFCQRActionsGetActionTemplate( $_REQUEST['type'], $action_id, $user_id );

	// Display template.
	echo $action_template;
}
