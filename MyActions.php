<?php
/**
 * My Actions
 *
 * @package NFC/QR Actions module
 */

require_once 'ProgramFunctions/Substitutions.fnc.php';
require_once 'modules/NFC_QR_Actions/includes/MyActions.fnc.php';

DrawHeader( ProgramTitle() );

if ( User( 'PROFILE' ) === 'teacher' )
{
	$_ROSARIO['allow_edit'] = true;
}

if ( $_REQUEST['modfunc'] === 'do_action' )
{
	require_once 'modules/NFC_QR_Actions/includes/DoAction.inc.php';
}

echo ErrorMessage( $error );

if ( $_REQUEST['modfunc'] === 'update' )
{
	if ( ! empty( $_REQUEST['values'] )
		&& ! empty( $_POST['values'] )
		&& AllowEdit() )
	{
		foreach ( (array) $_REQUEST['values'] as $id => $columns )
		{
			if ( ! empty( $columns['CLOSE_TAB_AFTER_SECONDS'] )
				&& (string) (int) $columns['CLOSE_TAB_AFTER_SECONDS'] !== $columns['CLOSE_TAB_AFTER_SECONDS'] )
			{
				// Fix SQL error invalid input syntax for type integer
				$error[] = _( 'Please enter valid Numeric data.' );

				continue;
			}

			if ( $id !== 'new' )
			{
				DBUpdate(
					'nfc_qr_staff_actions',
					$columns,
					[ 'ID' => (int) $id, 'STAFF_ID' => User( 'STAFF_ID' ) ]
				);
			}

			// New: check for Action.
			elseif ( $columns['ACTION_ID'] )
			{
				DBInsert(
					'nfc_qr_staff_actions',
					[ 'STAFF_ID' => User( 'STAFF_ID' ) ] + $columns
				);
			}
		}
	}

	// Unset modfunc, values & redirect URL.
	RedirectURL( [ 'modfunc' , 'values' ] );
}

if ( $_REQUEST['modfunc'] === 'remove'
	&& AllowEdit() )
{
	if ( DeletePrompt( dgettext( 'NFC_QR_Actions', 'Action' ), dgettext( 'NFC_QR_Actions', 'Remove' ) ) )
	{
		DBQuery( "DELETE FROM nfc_qr_staff_actions
			WHERE ID='" . (int) $_REQUEST['id'] . "'
			AND STAFF_ID='" . User( 'STAFF_ID' ) . "'" );

		// Unset modfunc & ID & redirect URL.
		RedirectURL( [ 'modfunc', 'id' ] );
	}
}

echo ErrorMessage( $error, 'error' );

if ( ! $_REQUEST['modfunc'] )
{
	$my_actions_RET = DBGet( "SELECT ID,ACTION_ID,STAFF_ID,CLOSE_TAB_AFTER_SECONDS
		FROM nfc_qr_staff_actions
		WHERE STAFF_ID='" . User( 'STAFF_ID' ) . "'
		ORDER BY ID",
	[
		'ACTION_ID' => 'NFCQRActionsMakeAction',
		'CLOSE_TAB_AFTER_SECONDS' => 'NFCQRActionsMakeNumberInput',
	] );

	$columns = [
		'ACTION_ID' => dgettext( 'NFC_QR_Actions', 'Action' ),
		'CLOSE_TAB_AFTER_SECONDS' => dgettext( 'NFC_QR_Actions', 'Close tab after (seconds)' ),
	];

	$link['add']['html'] = [
		'ACTION_ID' => NFCQRActionsMakeAction( '', 'ACTION_ID' ),
		'CLOSE_TAB_AFTER_SECONDS' => NFCQRActionsMakeNumberInput( '', 'CLOSE_TAB_AFTER_SECONDS' ),
	];

	$link['remove']['link'] = 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=remove';
	$link['remove']['variables'] = [ 'id' => 'ID' ];

	echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=update' ) . '" method="POST">';

	DrawHeader( '', SubmitButton() );

	ListOutput(
		$my_actions_RET,
		$columns,
		dgettext( 'NFC_QR_Actions', 'Action' ),
		dgettext( 'NFC_QR_Actions', 'Actions' ),
		$link
	);

	echo '<div class="center">' . SubmitButton() . '</div>';
	echo '</form>';
}
