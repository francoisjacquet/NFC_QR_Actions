/**
 * NFCQRActionsCopyURLJS() function JS
 *
 * @link https://stackoverflow.com/questions/53369140/copy-link-of-an-anchor-tag-to-clipboard-when-clicking-on-it#53369306
 *
 * @package NFC/QR Actions module
 */

$('.url-copy').on('click', function(e) {
	e.preventDefault();
	var copyText = $(this).attr('href');

	var copyEvent = function(e) {
		e.clipboardData.setData('text/plain', copyText);
		e.preventDefault();

		// Remove event so default copy can be done now.
		document.removeEventListener('copy', copyEvent, true);
	}

	document.addEventListener('copy', copyEvent, true);

	document.execCommand('copy');
	console.log('Copied URL: ', copyText);

	$(this).text($('#url_copied_text').val());
});
