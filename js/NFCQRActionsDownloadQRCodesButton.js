/**
 * NFCQRActionsDownloadQRCodesButton() function JS
 *
 * @package NFC/QR Actions module
 */

$('#download-zip-button').on('click', function() {
	nfcQrActionsDownloadZip('.qr-code-img');
});

/* Download all .output img in a zip file */
function nfcQrActionsDownloadZip(sel) {
	// Use jszip.min.js
	let zip = new JSZip();
	let zipFilename = 'QR_codes.zip';
	let images = document.querySelectorAll(sel);
	//console.log(images);

	images.forEach(function(image){
		// Reset image width to 300 so we don't get half of it.
		image.width = 300;

		// Get base64 data from img.
		let base64data = nfcQrActionsGetBase64Image(image);

		image.width = 150;

		let filename = image.id + '.png';
		zip.file(filename, base64data, { base64: true });
	});

	zip.generateAsync({ type: 'blob' }).then(function(content) {
		// Use FileSaver.min.js
		saveAs(content, zipFilename);
	});
}

/**
 * Get the image content, base64 encoded preferably,
 * without the need to redownload the image (ie. it's already loaded by the browser
 *
 * @link https://stackoverflow.com/questions/934012/get-image-data-url-in-javascript
 */
function nfcQrActionsGetBase64Image(img) {
	// Create an empty canvas element
	var canvas = document.createElement("canvas");
	canvas.width = img.width;
	canvas.height = img.height;

	// Copy the image contents to the canvas
	var ctx = canvas.getContext("2d");
	ctx.drawImage(img, 0, 0);

	// Get the data-URL formatted image
	// Firefox supports PNG and JPEG. You could check img.src to
	// guess the original format, but be aware the using "image/jpg"
	// will re-encode the image.
	var dataURL = canvas.toDataURL("image/png");

	return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}
