/**
 * DoAction.inc.php JS
 *
 * @package NFC/QR Actions module
 */

if ($('#nfc_qr_action_has_template').val() === 'Y') {
	/**
	 * Do action (once) in background.
	 *
	 * Add &action_done=Y to URL (after ajaxSuccess, no browser history).
	 * This way, we can reload template without doing action various times...
	 * @link https://stackoverflow.com/questions/20937280/how-to-change-url-without-changing-browser-history#20937387
	 */
	$.ajax($('#nfc_qr_action_url').val());
	setTimeout(function() {
		history.replaceState({}, document.title, document.URL + '&action_done=Y' );
	}, 10);

} else if ( $('#nfc_qr_action_url').val().indexOf('Modules.php?modname=') !== 0 ) {
	// Handle external URL.
	// Redirect to Action URL (no browser history).
	window.location.replace($('#nfc_qr_action_url').val());

} else {
	/**
	 * Redirect to Action URL (no browser history).
	 *
	 * @link https://stackoverflow.com/questions/20937280/how-to-change-url-without-changing-browser-history#20937387
	 *
	 * Replace current URL in history twice:
	 * 1. after current ajaxSuccess, but before action's AJAX, with Action URL
	 * 2. after action's AJAX, with X-Redirect-Url, so we remove modfunc
	 * This way, we can reload & go back in history without doing action various times...
	 */
	setTimeout(function() {
		var nfcQrActionUrl = $('#nfc_qr_action_url').val();

		history.replaceState({}, document.title, nfcQrActionUrl);

		var nfcQrActionAjaxOptions = ajaxOptions('body', nfcQrActionUrl, false);

		nfcQrActionAjaxOptions.success = function(data, s, xhr) {
			// See PHP RedirectURL().
			var redirectUrl = xhr.getResponseHeader("X-Redirect-Url");

			if (redirectUrl) {
				nfcQrActionUrl = redirectUrl;
				history.replaceState({}, document.title, nfcQrActionUrl);
			}

			ajaxSuccess(data, 'body', nfcQrActionUrl);
		};

		$.ajax(nfcQrActionUrl, nfcQrActionAjaxOptions);
	}, 10);
}
