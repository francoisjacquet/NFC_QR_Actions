/**
 * NFCQRActionsCloseTabAfterSecondsJS() function JS
 *
 * @package NFC/QR Actions module
 */

var closeTabAfterSeconds = function(seconds) {
	var nfcQrActionsUrl = document.URL;

	setTimeout(function() {
		// Refresh URL after ajaxLink in case of redirect.
		nfcQrActionsUrl = document.URL;
	}, 900);

	setTimeout(function() {
		// Cancel close if navigating: URL has changed.
		if (nfcQrActionsUrl === document.URL
			&& ! closeTabAfterSecondsCancelled) {
			window.close();
		}
	}, seconds * 1000);
};

if (typeof closeTabAfterSecondsCancelled === 'undefined') { // Run only once.
	closeTabAfterSecondsCancelled = false;
	closeTabAfterSeconds($('#close_tab_after_seconds').val());
}

$('#cancel_close_tab').on('click', function() {
	closeTabAfterSecondsCancelled = true;

	$(this).parent().hide();
});

// Do not display header on browser back if already cancelled.
if (closeTabAfterSecondsCancelled) {
	$('#cancel_close_tab').parent().hide();
}
